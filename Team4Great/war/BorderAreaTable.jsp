<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<title>All Registered Border areas</title>
</head>
	<body>
		<h1>Welcome</h1>
		<p>
			Here are is the table displaying data about current border controlled areas.
		</p>
			<c:choose>
				<c:when test="${not empty BorderAreasInSession}">
				<table>
				<tr>
				<th>Name of the Border area</th>
				<th>Location of the Border area</th>
				<th>Number of unnits positioned in the area</th>
				</tr>
				<c:forEach items="${BorderAreasInSession}" var="currentBorderArea">
					<tr>
					<td>${currentBorderArea.areaName}</td>
					<td>${currentBorderArea.address}</td>
					<td>${currentBorderArea.unitCount}</td>
					</tr>
				</c:forEach>
				</table>
					
				</c:when>
				<c:otherwise>
					<p>
						Piirivalve alasi ei leitud
					</p>
				</c:otherwise>
			</c:choose>
		
	</body>
</html>
