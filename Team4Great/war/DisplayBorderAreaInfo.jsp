<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Border area</title>
</head>
<body>
	<c:choose>
		<c:when test="${not empty BorderArea}">
		<p>Border area name: ${BorderArea.areaName}</p>
		<p>Border area location: ${BorderArea.address}</p>
		<p>Units working on the area: ${BorderArea.unitCount}</p>
		</c:when>
		<c:otherwise>
			<h1>Welcome to Border area registering system.</h1>
			<p>
			To enter a new border area to the system please follow this link
			</p>
			<a href="/Team4Great/EnterArea.html">Go to Registration</a>
		</c:otherwise>
	</c:choose>
</body>
</html>