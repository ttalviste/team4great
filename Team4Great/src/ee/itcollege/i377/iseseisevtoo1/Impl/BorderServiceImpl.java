package ee.itcollege.i377.iseseisevtoo1.Impl;

import java.util.ArrayList;
import java.util.List;

import ee.itcollege.i377.iseseisevtoo1.BO.BorderArea;
import ee.itcollege.i377.iseseisevtoo1.Service.BorderService;

public class BorderServiceImpl implements BorderService {

	@Override
	public List<BorderArea> GetAllBorderdAreas() {
		List<BorderArea> borderAreas = new ArrayList<BorderArea>();
		
		
		borderAreas.add(new BorderArea("Esimene","Narva",30));
		borderAreas.add(new BorderArea("Teine","Tallinn",10));
		borderAreas.add(new BorderArea("Kolmas","Paldiski",13));
		borderAreas.add(new BorderArea("Neljas","Kallaste",33));
		borderAreas.add(new BorderArea("Viies","Valga",1000));
		return borderAreas;
	}

	@Override
	public BorderArea GetBorderAreaByAreaName(String areaName) {
		List<BorderArea> areas = GetAllBorderdAreas();
		
		if (areas.toArray().length > 0) {
			for (int i = 0; i < areas.toArray().length; i++) {
				BorderArea area = areas.get(i);
				if (area.getAreaName() == areaName) {
					return area;
				}
			}
		}
		return null;
	}

	

}
